## Key Tasks to Complete

* [ ] Step 1: Create a new Merge Request
  * Still on our issue view board, go ahead and click the issue. 
  * Now in the issue view we want to click **Create merge request**. **Please note:** if you did not remove the fork relationship before this option will not be shown.
  * On the resulting _New Merge Request_ form we first want to uncheck **Mark as draft**.
  * You can then leave all other settings as is. Scroll to the bottom of the form and click **Create merge request**

* [ ] Step 2: Open The Web IDE
  * Now in our Merge Request view notice how everything is still tied back to the orginal issue we created.
  * We then want to go ahead and click **Code**, then **Open in Web IDE** in the resulting dropdown.
  * We know from our seperate security reporting tool that the token leak occured at **Lib/tanukiracing/app.rb**.
  * Once in _app.rb_ go ahead and remove the token leak on line 20.
  * Next click the **Source control** button, add a quick commit message, then click **Commit..**
  * A pop up will then apear where we want to click **Go to MR**
  * Now that we are back in our merge request view notice that the **Changes** tab has populated with our code fix

* [ ] Step 3: GitLab's One Platform Benefit
  * After showing the team the fix, the project manager wants you to set up GitLab CICD and Security so that these problems can be prevented at the root, never leaving GitLab.
  * We first are going to set up a CICD pipeline to run our Security scans. Use the left hand navigation menu to click through **Build > Pipeline editor**
  * In the top left ensure we are in our branch related to the issue we created, then click **configure pipeline**
  * We are then going to want to clear out the example pipeline GitLab has populated for us, and paste in the code snipet below:

    ```
    image: docker:latest

    services:
      - docker:dind

    variables:
      CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
      DOCKER_DRIVER: overlay2
      ROLLOUT_RESOURCE_TYPE: deployment
      DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
      RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
      DAST_BAS_DISABLED: "true"
      CI_DEBUG_TRACE: "true"
      # CI_PROJECT_PATH_SLUG: "tanukiracing"
      

    stages:
      - build
      - test

    include:
      - template: Jobs/Test.gitlab-ci.yml
      - template: Jobs/Container-Scanning.gitlab-ci.yml
      - template: Code-Quality.gitlab-ci.yml
      - template: Jobs/Dependency-Scanning.gitlab-ci.yml
      - template: Jobs/SAST.gitlab-ci.yml
      - template: Jobs/Secret-Detection.gitlab-ci.yml
      - template: Jobs/SAST-IaC.gitlab-ci.yml

    build:
      stage: build
      variables:
        IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
      script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build -t $IMAGE .
        - docker push $IMAGE

    sast:
      variables:
          SEARCH_MAX_DEPTH: 12

    ```
  * Note the number of CICD tools that GitLab gives you to help define your pipeline. Spend some time checking out the **Visualize, Validate, and Full configuration** tabs.
  * We then are going to want to click **Commit changes** to kick off our security pipeline. Because we made this change on the Merge Request's branch, we can watch the pipeline run there. Use the left hand naviagtion menu to click through **Code > Merge requests** 

  > Your instructor may have you go to a quick break while this pipeline runs
